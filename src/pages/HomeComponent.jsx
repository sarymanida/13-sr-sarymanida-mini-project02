
import React, { useEffect, useState } from 'react'
import { Button, Col, Container, Form, Row } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import { api } from '../api/api';
import Swal from "sweetalert2";
import CardComponent from '../components/CardComponent';

export default function HomeComponent() {

    const [data, setData] = useState([]);
  
    useEffect(() => {
      api.get("/articles").then((res) => {
        setData(res.data.payload);
      });
    }, []);
  
    const handleDelete = (id) => {
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
            confirmButton: 'btn btn-success ',
            cancelButton: 'btn btn-danger '
        },
        buttonsStyling: false,
      });
  
      swalWithBootstrapButtons
        .fire({
          title: "Are you sure?",
          text: "You won't be able to revert this!",
          icon: "warning",
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, cancel!',
          showCancelButton: true,
          showCloseButton: true
        })
        .then((result) => {
          let message = "Your file has been deleted.";
          if (result.isConfirmed) {
            const newData = data.filter((data) => data._id !== id);
            setData(newData);
            api.delete(`/articles/${id}`).then((r) => {
              message = r.data.message;
            });
  
            swalWithBootstrapButtons.fire("Deleted!", message, "success");
          } else {
            return;
          }
        });
    };
    return (
      <div>
        <Container>
          <div className="d-flex p-3 justify-content-between">
            <h1>All Articles</h1>
            <Button
              as={Link}
              to="/create"
              className="btn-dark text-center align-self-center fw-bold"
            >
              New Article
            </Button>
          </div>
          <Row>
            {data.map((item, index) => (
              <Col xs={6} sm={3} md={2} key={index}>
                <CardComponent item={item} handleDelete={handleDelete} />
              </Col>
            ))}
          </Row>
        </Container>
      </div>
    );
}
