import React from "react";
import { Button, Col, Container, Image, Row, Table } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router-dom";

export default function ViewComponent() {
  const location = useLocation();
  const oldData = location.state;
  const navigate = useNavigate();
  return (
    <Container className="my-5 p-3 border border-dark rounded-2">
      <Row>
        <Col lg={12}>
          <Button
            onClick={() => navigate("/")}
            style={{ width: "10%" }}
            className="m-3 btn-dark fw-bold"
          >
            Back
          </Button>
        </Col>
      </Row>
      <Row>
        <Col lg={12}>
          <Container>
            <Row>
              <Col lg={6} sm={6} md={6}>
                <div style={{ display: "flex", margin: 5, height: "500px" }}>
                  <Image
                    src={
                      oldData.image ??
                      "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png?w=640"
                    }
                    width="50%"
                    height="100%"
                    style={{
                      flex: 1,
                      borderRadius: "20px",
                      objectFit: "cover",
                    }}
                  />
                </div>
              </Col>
              <Col lg={6} sm={6} md={6}>
                <div>
                  <Table striped bordered hover variant="dark">
                    <tbody>
                      <tr>
                        <td>
                          <h1> Title: {oldData.title}</h1>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <h4>Description: {oldData.description}</h4>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          <h4>
                            Published: {oldData.published ? "True" : "False"}
                          </h4>
                        </td>
                      </tr>
                    </tbody>
                  </Table>
                </div>
              </Col>
            </Row>
          </Container>
        </Col>
      </Row>
    </Container>
  );
}
