
import React, { useState } from 'react'
import { Button, Container, Form } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { api } from '../api/api';

export default function CreateCategory() {
    const [name, setTitle] = useState("");
    const navigate = useNavigate();
    const submit = () => {
      Swal.fire({
        title: "Are you sure?",
        text: "You will saved this Category!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, save it!",
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire("Save!", "Your file has been Save.", "success");
          handleSubmit();
          navigate("/category");
        } else {
          return;
        }
      });
    };
    const handleSubmit = () => {
      api
        .post("/category", { name })
        .then((res) => (res.data.message));
    };
  
    const handleNameChange = (e) => {
      setTitle(e.target.value);
    };
    return (
      <Container className="w-50 pt-5">
        <h1>Add New Category</h1>
        <Form>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Title</Form.Label>
            <Form.Control
              type="text"
              placeholder="name"
              onChange={handleNameChange}
            />
          </Form.Group>
          <Button variant="primary" onClick={submit}>
            Submit
          </Button>
        </Form>
      </Container>
    );
}
