import React from 'react'
import { Button, Container, Image, Row } from 'react-bootstrap';
import { useLocation, useNavigate } from 'react-router-dom';

export default function ViewCategory() {
    const location = useLocation();
    const oldData = location.state;
    console.log(oldData);
    const navigate = useNavigate();
    return (
      <>
        <Container className="my-2">
          <Row className="view-card p-3">
            <Button
              onClick={() => navigate("/category")}
              style={{ width: "10%" }}
              className="m-3 btn-dark fw-bold"
            >
              Back
            </Button>
            <div
              className=""
              style={{ display: "flex", margin: 5, height: "500px" }}
            >
              <h1 className="p-3">Category Name: </h1>
              <h1 className="p-3"> {oldData.name}</h1>
            </div>
          </Row>
        </Container>
      </>
    );
}
