
import React, { useEffect, useState } from 'react'
import { Button, Container, Form } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import Swal from "sweetalert2";
import { api } from '../api/api';

export default function CreateCardComponent() {
    const [title, setTitle] = useState("");
    const [description, setDescription] = useState("");
    const [isPublished, setIsPublished] = useState(true);
    const [image, setImage] = useState(
      "https://www.pulsecarshalton.co.uk/wp-content/uploads/2016/08/jk-placeholder-image.jpg",
    );
    const [imageUrl, setImageUrl] = useState();
    const [category, setCategory] = useState("");
  
    const navigate = useNavigate();
  
    const handleSubmit = () => {
      api
        .post("/articles", { title, description, isPublished, image, category })
        .then((res) => (res.data.message));
    };

    const submit = () => {
        Swal.fire({
          title: "Are you sure?",
          text: "This article will be saved and showed",
          icon: "warning",
          showCancelButton: true,
          confirmButtonColor: "#3085d6",
          cancelButtonColor: "#d33",
          confirmButtonText: "Yes, save it!",
        }).then((result) => {
          if (result.isConfirmed) {
            Swal.fire("Save!", "Your file has been Save.", "success");
            handleSubmit();
            navigate("/");
          } else {
            return;
          }
        });
      };
  
    const handleTitleChange = (e) => {
      setTitle(e.target.value);
    };
    const handleDesChange = (e) => {
      setDescription(e.target.value);
    };
  
    const isPublishedChange = (e) => {
      setIsPublished(e.target.checked);
    };
  
    const handleImageChange = (e) => {
      setImageUrl(URL.createObjectURL(e.target.files[0]));
      const formData = new FormData();
      formData.append("image", e.target.files[0]);
      api
        .post("/images", formData)
        .then((res) => setImage(res.data.payload.url));
    };
  
    return (
      <Container className="w-50 pt-5">
        <h1>Add New Article</h1>
        <Form>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Title</Form.Label>
            <Form.Control
              type="text"
              placeholder="title"
              onChange={handleTitleChange}
            />
          </Form.Group>
          <Form.Group className="mb-3" controlId="formBasicPassword">
            <Form.Label>Description</Form.Label>
            <Form.Control
              type="text"
              value={description}
              placeholder="description"
              onChange={handleDesChange}
            />
          </Form.Group>
          <Form.Group controlId="formFile" className="mb-3">
            <Form.Label>Choose Image</Form.Label>
            <Form.Control type="file" onChange={handleImageChange} />
          </Form.Group>
          <img
            src={imageUrl ?? image}
            alt="preview"
            style={{ height: "200px" }}
          />
          <Form.Group className="mb-3" controlId="formBasicCheckbox">
            <Form.Check
              type="checkbox"
              checked={isPublished}
              label="Is Published"
              onChange={isPublishedChange}
            />
          </Form.Group>
          <Button variant="primary" onClick={submit}>
            Submit
          </Button>
        </Form>
      </Container>
    );
}