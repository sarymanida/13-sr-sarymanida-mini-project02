import React, { useEffect, useState } from 'react'
import { Button, Container, Form } from 'react-bootstrap';
import { useLocation, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import { api } from '../api/api';

export default function UpdateCategory() {
    const [name, setName] = useState("");
    const [id, setId] = useState();
    const navigate = useNavigate();
  
    const submit = () => {
      Swal.fire({
        title: "Are you sure?",
        text: "You will update this category!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, save it!",
      }).then((result) => {
        if (result.isConfirmed) {
          Swal.fire("Save!", "Your file has been Save.", "success");
          handleSubmit();
          navigate("/category");
        } else {
          return;
        }
      });
    };
  
    const handleSubmit = () => {
      api
        .put("/category/" + id, { name })
        .then((res) => (res.data.message));
    };
  
    const handleNameChange = (e) => {
      setName(e.target.value);
    };
  
    const location = useLocation();
  
    useEffect(() => {
      setName(location.state.name);
      setId(location.state.id);
    }, [location]);
  
    return (
      <Container className="w-50">
        <h1>Add New Category</h1>
        <Form>
          <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Title</Form.Label>
            <Form.Control
              value={name}
              type="text"
              placeholder="name"
              onChange={handleNameChange}
            />
          </Form.Group>
          <Button variant="primary" onClick={submit}>
            Submit
          </Button>
        </Form>
      </Container>
    );
}
