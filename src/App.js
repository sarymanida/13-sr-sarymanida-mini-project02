import "./App.css";
import NavComponent from "./components/NavComponent";
import { Route, Routes } from "react-router-dom";
import HomeComponent from "./pages/HomeComponent";
import CreateCardComponent from "./pages/CreateCardComponent";
import UpdateComponent from "./pages/UpdateComponent";
import ViewComponent from "./pages/ViewComponent";
import CreateCategory from "./pages/CreateCategory";
import ViewCategory from "./pages/ViewCategory";
import NoFoundPage from "./components/NoFoundPage";
import UpdateCategory from "./pages/UpdateCategory";
import CategoriesComponent from "./pages/CategoriesComponent";

function App() {
  return (
    <div className="App">
      <NavComponent />
      <Routes>
        <Route path="/" element={<HomeComponent />} />
        <Route path="/create" element={<CreateCardComponent />} />
        <Route path="/update" element={<UpdateComponent />} />
        <Route path="/view" element={<ViewComponent />} />

        <Route path="/category" element={<CategoriesComponent/>} />
        <Route path="/category/create" element={<CreateCategory/>} />
        <Route path="/category/update" element={<UpdateCategory/>} />
        <Route path="/category/view" element={<ViewCategory />} />

        <Route path="*" element={<NoFoundPage/>} />
        
      </Routes>
    </div>
  );
}

export default App;
