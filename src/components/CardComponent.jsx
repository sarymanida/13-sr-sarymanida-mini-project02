import React from "react";
import { Button, Card } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

export default function CardComponent({ item, handleDelete }) {
  const navigate = useNavigate();
  const handleUpdate = () => {
    navigate("/update", { state: { ...item } });
  };

  const handleView = () => {
    navigate("/view", { state: { ...item } });
  };
  return (
      <Card className="my-2 " style={{ borderRadius: "20px" }}>
        <Card.Img
          variant="top"
          style={{ height: "200px", objectFit: "cover", borderRadius: "20px" }}
          className="mb-2 p-2"
          src={
            item.image ??
            "https://socialistmodernism.com/wp-content/uploads/2017/07/placeholder-image.png?w=640"
          }
        />
        <Card.Body>
          <Card.Title> <h1>{item.title}</h1> </Card.Title>
          <Card.Text className="text" style={{ height: "100px" }}>
            {item.description}
          </Card.Text>
          <div className="d-flex flex-column mb-2">
            <Button className="mb-2" onClick={handleView} variant="primary">
              View
            </Button>
            <Button
              className="mb-2"
              variant="danger"
              onClick={() => handleDelete(item._id)}
            >
              Delete
            </Button>
            <Button variant="info" onClick={() => handleUpdate(item._id)}>
              Update
            </Button>
          </div>
        </Card.Body>
      </Card>
  );
}
