import React from "react";
import { Button, Card, Container } from "react-bootstrap";
import { useNavigate } from "react-router-dom";

export default function CategoryComponent({ item, handleDelete }) {
  const navigate = useNavigate();
  const onUpdate = (name, id) => {
    navigate("/category/update", { state: { name, id} });
  };

  const onView = () => {
    navigate("/category/view", { state: { ...item } });
  };

  return (
    <Container>
      <Card className="my-2 pt-3" style={{ borderRadius: "20px", width: "13rem" }}>
        <Card.Body>
          <Card.Title style={{ height: "50px" }} className="title">
            <h2 style={{  textAlign: "center"}} >
            {item.name}
            </h2>  
          </Card.Title>
          <div className="d-flex flex-column mb-2">
            <Button className="mb-2" onClick={onView} variant="primary">
              View
            </Button>
            <Button
              className="mb-2"
              variant="danger"
              onClick={() => handleDelete(item._id)}
            >
              Delete
            </Button>
            <Button variant="info" onClick={() => onUpdate(item.name, item._id)}>
              Update
            </Button>
          </div>
        </Card.Body>
      </Card>
    </Container>
  );
}
