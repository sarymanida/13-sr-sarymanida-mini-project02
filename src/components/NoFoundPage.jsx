import React from 'react'

export default function NoFoundPage() {
  return (
    <div className='alignCenter-middle'>
        <h1>There is no page with this path name !</h1>
        <h1>Please choose the correct URL.</h1>
    </div>
  )
}
